import { TodoModel } from '../models/todo-model';

export class CategoryModel {

  constructor(
    public id: number,
    public text: string,
    public todos: TodoModel[]
  ) {}
}
