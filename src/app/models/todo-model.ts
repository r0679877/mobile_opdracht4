export class TodoModel {

    public id: number;
    public text: string;
    public status: boolean;

    constructor(pId: number, pText: string, pStatus: boolean) {
        this.id = pId;
        this.text = pText;
        this.status = pStatus;
    }
}
