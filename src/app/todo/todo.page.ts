import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { TodoModel } from '../models/todo-model';
import { CategoryModel } from '../models/category-model';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.page.html',
  styleUrls: ['./todo.page.scss'],
})
export class TodoPage implements OnInit {

  constructor(private route: ActivatedRoute, private storage: Storage, public alertController: AlertController) { }

  id;
  category: CategoryModel;
  todos: TodoModel[] = [];

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.storage.ready().then(() => {
      const pId: string = 'C' + this.id;
      this.storage.get(pId).then((val) => {
        this.category = val;
        this.loadTodos(val.todos);
      });
    });
  }

  loadTodos(pTodos: TodoModel[]) {
    this.todos = pTodos;
  }

  addTodo(pText: string) {
    const count = this.todos.length;
    const cId: string = 'C' + this.id.toString();

    this.category.todos.push({id: count, text: pText, status: false });
    this.storage.set(cId, this.category);
  }
  deleteTodo(pTodo: TodoModel) {
    const cId: string = 'C' + this.id.toString();
    const index: number = this.todos.indexOf(pTodo);
    if (index !== -1) {
      this.todos.splice(index, 1);
      this.category.todos = this.todos;
      this.storage.set(cId, this.category);
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Add new todo',
      inputs: [
        {
          name: 'newTodo',
          type: 'text',
          placeholder: 'Todo'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Add',
          handler: data => {
            this.addTodo(data.newTodo);
          }
        }
      ]
    });

    await alert.present();
  }
}
