import { Component } from '@angular/core';
import { CategoryModel } from '../models/category-model';
import { TodoModel } from '../models/todo-model';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  constructor(public alertController: AlertController, private storage: Storage, private router: Router) {}

  count;
  todos: TodoModel[] = [];
  categories: CategoryModel[] = [];

  ionViewDidEnter() {
    this.storage.ready().then(() => {
      this.loadCategories();
    });
  }

  goToTodoPage(selectedId: string) {
    this.router.navigate(['todo/' + selectedId]);
  }

  loadCategories() {
    this.storage.keys().then((val) => {
      console.log(val);
      for (const i of val) {
        this.storage.get(i).then((category) => {
          this.categories.push(category);
        });
      }
    });
  }

  addCategory(pText: string) {
    const pId: number = this.categories.length;
    const cId: string = 'C' + pId.toString();
    const newCategory = new CategoryModel(pId, pText, this.todos);

    this.storage.set(cId, newCategory).then((item) => {
      this.categories[pId] = item;
    });
  }
  deleteCategory(pCategory: CategoryModel) {
    const cId: string = 'C' + pCategory.id.toString();

    const index: number = this.categories.indexOf(pCategory);
    if (index !== -1) {
      this.categories.splice(index, 1);
      this.storage.ready().then(() => {
        this.storage.remove(cId).then(() => {
        });
      });
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Add new category',
      inputs: [
        {
          name: 'newCategory',
          type: 'text',
          placeholder: 'Category'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Add',
          handler: data => {
            this.addCategory(data.newCategory);
          }
        }
      ]
    });

    await alert.present();
  }
}
